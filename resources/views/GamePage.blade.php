@extends('layouts.masterLayouts')

@section('title')
	{{ $nice_name }}
@endsection

@section('content')
	<a class="home" href="/" title="Click here for more games"></a>
	<div class="game-wrapper">
		<div>
			<h1>{{ $nice_name }}</h1>
		</div>
		<div class="game-content">
			<iframe class= "game-iframe" src="{{ $url }}"></iframe>
		</div>
	</div>

	<div style="height:20px;background-color:#4a311c;"></div>

	<div class="outer-content">
		<div class="social-wrapper">
			<div class="average-rating">
				<span>The Average Rating :</span>
				<span id="averageRate">{{ $rating }}%</span>
				<span id="averageRateIcon" class="{{( $rating  >= 50 ? 'thumbup' : 'thumbdown' )}}"></span>
			</div>
			<div style="height:15px;"></div>
			@guest
			<div class="user-rate">
				<span>If you want to rate this game, please log in!</span>
			</div>
			@endguest
			@auth
			<div class="user-rate">
				<span>How do you want to rate this game?</span>
				<div>
					<a href="#" class="thumbup" id="thumbup"></a>
					<a href="#" class="thumbdown" id="thumbdown"></a>
				</div>
				<span id="userRated">{{ $rated }}</span>
			</div>
			@endauth
		</div>
	</div>
@endsection

@section('script')
<script type="text/javascript" src="/js/app.js"></script>
<script>
	$.ajaxSetup
	({
		headers:
		{
		   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

	$('.thumbup').click(function(event)
	{
		console.log('THUMBUP');
		event.preventDefault();

		sendAjaxRequest('{{ route('thumbup') }}', 1);
	})

	$('.thumbdown').click(function(event)
	{
		console.log('THUMBDOWN');
		event.preventDefault();

		sendAjaxRequest('{{ route('thumbdown') }}', 0);
	})

	var sendAjaxRequest = function(url, rating)
	{
		jQuery.ajax
		({
			method: 'POST',
			url: url,
			data: {rating: rating, gameid: {{ $gameid }}},
			success: function(data)
			{
				document.getElementById("userRated").textContent ="You have made " + (rating ? "an up" : "a down") + "vote!";
				document.getElementById("averageRate").textContent= data.current_rating +"%";

				if (data.current_rating >= 50)
				{
					document.getElementById("averageRateIcon").className = 'thumbup';
				}
				else
				{
					document.getElementById("averageRateIcon").className = 'thumbdown';
				}
			}
		});
	}
</script>
@endsection
