@extends('adminPanel.layouts.adminPanelLayout')

@section('title')
	Admin Management
@endsection

@section('content')
<nav id="sidebar">
	<!-- Sidebar Links -->
	<ul class="list-unstyled components">
		<li><a href="/admin/">Authentication</a></li>
		<li><a href="/admin/">Add/Update</a></li>
	</ul>
</nav>
<div class="content">
  <h2>This site shows all the accounts which can be used to log into admin dashboard </h2>
  <p>Show user authentication</p>
</div>
@endsection