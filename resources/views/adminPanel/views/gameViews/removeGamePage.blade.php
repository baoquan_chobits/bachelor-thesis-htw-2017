@extends('adminPanel.views.GamePage')

@section('gameContent')
	<form action="/admin/game/deleteGame" method="POST" role="form">
		{{ csrf_field() }}
		
		 @if ($flash = session('game-deleted-message'))
					<div id="flash-message" class="alert alert-warning" role="alert">
						{{ $flash }}
					</div>
		 @endif
			
		<div style="margin-left:150px;color:#0080c0"><h1>REMOVE GAMES</h1></div>
		
		<input type="submit" name="delete-game" id="delete-game" class="btn btn-danger" style="margin-left:1180px" value="Delete">
		
		<div style="height:80px"></div>
		
		<div class="table-responsive" style="width:1200px;margin-left:150px">
			<table class="table">
				<thead>
					<tr>
						<th class="col-sm-1 text-center">Game Number</th>
						<th class="col-sm-2 text-center">Game Icon</th>
						<th class="col-sm-2 text-center">Game Name</th>
						<th class="col-sm-2 text-center">Catalog</th>
						<th class="col-sm-2 text-center">Check to delete</th>
					</tr>
				</thead>
				@foreach ($allGames as $allGames)
				<tbody>
					<tr>
						<td class="text-center">
							{{ $allGames->id }}
						</td>
						<td class="text-center">
							<img src="/images/gameIcon/{{ $allGames->game_name }}.jpg" style="height:86px">
						</td>
						<td class="text-center">
							{{ $allGames->nice_name }}
						</td>
						<td class="text-center">
							{{ $allGames->catalog }}
						</td>
						<td class="text-center">
							<input type="checkbox" id="{{ $allGames->game_name }}" value="{{ $allGames->game_name }}" name='checkboxes[]'>
						</td>
					</tr>
				</tbody>
				@endforeach
			</table>
		</div>
	</form>
@endsection

@section('script')

	function getSelectedText(elementId) {
	    var elt = document.getElementById(elementId);
	
	    if (elt.selectedIndex == -1)
	        return null;
	
	    return elt.options[elt.selectedIndex].text;
	}
	
	$.ajaxSetup
	({
		headers: 
		{
		   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$(document).ready(function(){
		$("select.gameList").change(function () {
			var game = getSelectedText('gameList');
			event.preventDefault();
			jQuery.ajax({
		        type: 'POST',
		        url: '/selectGame',
				data: {game: game},
		        success: function (data) 
		        {
					document.getElementById("backend-name").value= data.game_name;
					document.getElementById("nice-name").value= data.nice_name;
					document.getElementById("game-url").value= data.game_url;
					document.getElementById("catalog").value= data.catalog;
		        }
		    });
		});
	})
		
@endsection