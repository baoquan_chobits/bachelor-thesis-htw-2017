@extends('adminPanel.views.GamePage')

@section('gameContent')
	<div>
		<h1 style="margin-left:150px;color:#0080c0">ALL GAMES</h1>
		<div style="height:80px"></div>
	</div>
	<div class="table-responsive" style="width:1200px;margin-left:150px">
		<table class="table">
			<thead>
				<tr>
					<th class="col-sm-1 text-center">Game Number</th>
					<th class="col-sm-2 text-center">Game Icon</th>
					<th class="col-sm-2 text-center">Game Name</th>
					<th class="col-sm-2 text-center">Catalog</th>
				</tr>
			</thead>
			@foreach ($allGames as $allGames)
			<tbody>
				<tr>
					<td class="text-center">
						{{ $allGames->id }}
					</td>
					<td class="text-center">
						<img src="/images/gameIcon/{{ $allGames->game_name }}.jpg" style="height:86px">
					</td>
					<td class="text-center">
						{{ $allGames->nice_name }}
					</td>
					<td class="text-center">
						{{ $allGames->catalog }}
					</td>
				</tr>
			</tbody>
			@endforeach
		</table>
	</div>
@endsection