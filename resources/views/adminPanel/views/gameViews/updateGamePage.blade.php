@extends('adminPanel.views.GamePage') 

@section('gameContent')
	<div>
		<h1 style="margin-left: 150px; color: #0080c0">UPDATE GAME</h1>
		<div style="height: 80px"></div>
	</div>
	<div>
		<form action="/admin/game/updateGame" method="POST" role="form">
			{{ csrf_field() }}
			
			 @if ($flash = session('game-updated-message'))
					<div id="flash-message" class="alert alert-warning" role="alert">
						{{ $flash }}
					</div>
			 @endif
			  
			 <div class="form-group">
				<label class="col-sm-2 control-label" style="width:14%">Choose Game</label>
				<select class="form-control gameList" style="width:20%" id="gameList">
					<option disabled selected value> -- Select a game -- </option>
					@foreach ($allGames as $allGames)
						<option>{{ $allGames->nice_name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Backend Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="backend-name" name="backend-name" placeholder="backend_game_name" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nice Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="nice-name" name="nice-name" placeholder="Nice Game Name" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Game URL</label>
				<div class="col-sm-10">
					<input type="url" class="form-control" id="game-url" name="game-url" placeholder="www.fakeUrl.com" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Catalog</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="catalog" name="catalog" placeholder="casual/fighting/adventure..." required>
				</div>
			</div>
			<div style="margin-left:360px;padding-top:120px;">
				<input type="submit" name="create-game" id="update-game" class="btn btn-success" value="Update">
			</div>
		</form>
	
	</div>
	<div style="height: 80px"></div>
@endsection
@section('script')

	function getSelectedText(elementId) {
	    var elt = document.getElementById(elementId);
	
	    if (elt.selectedIndex == -1)
	        return null;
	
	    return elt.options[elt.selectedIndex].text;
	}
	
	$.ajaxSetup
	({
		headers: 
		{
		   'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});
	$(document).ready(function(){
		$("select.gameList").change(function () {
			var game = getSelectedText('gameList');
			event.preventDefault();
			jQuery.ajax({
		        type: 'POST',
		        url: '/selectGame',
				data: {game: game},
		        success: function (data) 
		        {
					document.getElementById("backend-name").value= data.game_name;
					document.getElementById("nice-name").value= data.nice_name;
					document.getElementById("game-url").value= data.game_url;
					document.getElementById("catalog").value= data.catalog;
		        }
		    });
		});
	})
		
@endsection