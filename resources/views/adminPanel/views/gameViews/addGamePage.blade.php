@extends('adminPanel.views.GamePage') 

@section('gameContent')
	<div>
		<h1 style="margin-left: 150px; color: #0080c0">ADD GAME</h1>
		<div style="height: 80px"></div>
	</div>
	<div>
		<form action="/admin/game/createNewGame" method="POST" role="form">
			{{ csrf_field() }}
			
			 @if ($flash = session('game-added-message'))
					<div id="flash-message" class="alert alert-warning" role="alert">
						{{ $flash }}
					</div>
			 @endif
			  
			<div class="form-group">
				<label class="col-sm-2 control-label">Backend Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="backend-name" name="backend-name" placeholder="backend_game_name" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Nice Name</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="nice-name" name="nice-name" placeholder="Nice Game Name" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Game URL</label>
				<div class="col-sm-10">
					<input type="url" class="form-control" id="game-url" name="game-url" placeholder="www.fakeUrl.com" required>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">Catalog</label>
				<div class="col-sm-10">
					<input type="text" class="form-control" id="catalog" name="catalog" placeholder="casual/fighting/adventure..." required>
				</div>
			</div>
			<div style="margin-left:360px;padding-top:120px;">
				<input type="submit" name="create-game" id="create-game" class="btn btn-success" value="Add Game">
			</div>
		</form>
	
	</div>
@endsection