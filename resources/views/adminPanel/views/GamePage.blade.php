@extends('adminPanel.layouts.adminPanelLayout')

@section('title')
	Games Management
@endsection

@section('content')
<nav id="sidebar">
	<!-- Sidebar Links -->
	<ul class="list-unstyled components">
		<li><a href="/admin/games">All Games</a></li>
		<li><a href="/admin/addgame">Add Game</a></li>
		<li><a href="/admin/game/config">Config Game</a></li>
		<li><a href="/admin/game/remove">Delete Games</a></li>
	</ul>
</nav>

<div class="content">
	 @yield('gameContent')
</div>
@endsection