<!DOCTYPE html>
<html lang="en">
<head>
 	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
 	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="/favicon.ico" rel="icon" type="image/x-icon" />
  	<title>@yield('title')</title>
	<link rel="stylesheet" href="/css/adminPanel.css">
</head>

<body>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="/admin">Admin Dashboard</a>
    </div>
    <ul class="nav navbar-nav">
      <li><a href="/admin">Admin</a></li>
      <li><a href="/admin/user">User</a></li>
      <li><a href="/admin/games">Game</a></li>
    </ul>
  </div>
</nav>
@yield('content')
</body>

<script type="text/javascript" src="/js/app.js"></script>
<script>
@yield('script')
</script>
</html>