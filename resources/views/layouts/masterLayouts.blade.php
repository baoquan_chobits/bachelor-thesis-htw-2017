<!DOCTYPE html>
<html lang="en">
<head>
 	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
 	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link href="/favicon.ico" rel="icon" type="image/x-icon" />
  	<title>@yield('title')</title>
	<link rel="stylesheet" href="/css/app.css">
</head>

<body>

	@include('view.newPasswordOverlay')
	@include('view.registrationOverlay')
	@include('view.forgottenPasswordOverlay')
	@include('view.loginHeader')

	<div class="outer-content" style="padding: 50px 0">
			@yield('content')
	</div>

    @include('view.footer')
    @yield('script')
	<script type="text/javascript">

		$ = function(id)
		{
		  return document.getElementById(id);
		}

		var show = function(id)
		{
			$(id).style.display ='block';
		}
		var hide = function(id)
		{
			$(id).style.display ='none';
		}
	</script>
</body>
</html>
