@extends('layouts.masterLayouts')

@section('title', 'Chobits Game Portal !')

@section('content')
		@if ($flash = session('success-sending-email'))
			<div id="flash-message" style="position: absolute;display:block;width:fit-content;top:11.1%;left:55%;animation-duration:6s;" class="alert alert-warning" role="alert">
				{{ $flash }}
			</div>
		@endif
		<div class="inner-content">
			@foreach ($allGames as $allGames)
    		<a class="game-block" href="/game/1/casual/{{ $allGames->nice_name }}" target="_self" title="{{ $allGames->nice_name }}">
				<span>
					<img class="game-icon-block" src="images/gameIcon/{{ $allGames->game_name }}.jpg">
				</span>
				<p class="game-text-block">{{ $allGames->nice_name }}</p>
			</a>
			@endforeach
		</div>
@endsection
