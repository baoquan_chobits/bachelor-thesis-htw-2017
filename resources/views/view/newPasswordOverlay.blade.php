<div class="container row panel panel-heading overlay" id="newPasswordOverlay">
	<h3 class="text-center" style="padding-bottom: 20px;color:#0783be;">Forgotten Password</h3>
	<form id="reset-password-form" action="/forgot/token-confirmed" method="POST" role="form">
		{{ csrf_field() }}
		
		@if ($flash = session('wrong-new-reset-password-input'))
			<div id="flash-message" style="position: absolute;display:block;width:fit-content;top:-25%;left:69%;" class="alert alert-warning" role="alert">
				{{ $flash }}
				<?php
  					echo "<script> window.onload = function()
					{
					    document.getElementById('password-reset').click();
					} 
					</script>";
  				?>
			</div>
		@endif
		
		@if ($flash = session('trigger-overlay-reset-password'))
				<?php
  					echo "<script> window.onload = function()
					{
					    document.getElementById('password-reset').click();
					} 
					</script>";
  				?>
		@endif
		
		<div class="form-group">
			<input type="password" name="password" id="password" tabindex="3" class="form-control" placeholder="Password" required>
		</div>
		<div class="form-group">
			<input type="password" name="password_confirmation" id="password_confirmation" tabindex="3" class="form-control" placeholder="Confirm Password" required>
		</div>
		<div class="form-group row col-sm-6 col-sm-offset-3">
			<input type="submit" name="reset-password-submit" tabindex="4" class="form-control btn btn-success" value="Reset Password">
		</div>
		<a style="top:-195px" class="close" onclick="hide('newPasswordOverlay')"></a>
	</form>
</div>
