<div class="footer center-block">
	     <div class="col-md-6">
	      	<h3>Games, Friends, Fun</h3>
	      	<p>Connect with your friends at Plinga! We offer a huge selection of fun, interactive, and entertaining social games.</p>
	     </div>
	      		
	     <div class="col-md-6">
      		<a class="footer-address" href="https://www.google.de/maps/place/%C4%90%E1%BA%A1i+h%E1%BB%8Dc+Khoa+h%E1%BB%8Dc+%E1%BB%A8ng+d%E1%BB%A5ng+Kinh+t%E1%BA%BF+v%C3%A0+K%E1%BB%B9+thu%E1%BA%ADt+Berlin/@52.4931991,13.5232669,17z/data=!3m1!4b1!4m5!3m4!1s0x47a8492e8e935b15:0xa0d5d0da2ff283a6!8m2!3d52.4931991!4d13.5254556" target="_blank" title="Google Maps URL">
                               	HTW Berlin Wirtschaftsinformatik
                                <br>
                                Treskowallee 8
                                <br>
                                10318 Berlin
                                <br>
                                Germany
          	</a>
	      </div>

	    <div class="col-md-8 footer-colophon" style="padding-top: 20px">
	       <span style="padding-right: 30px;">
	              Copyright 2001 -
	              2017 Plinga GmbH
	       </span>
	       
	       <span>
	        	<a class="footer-colophon" href="http://www.plinga.com/corporate/" target="_blank" title="Corporate">Corporate</a>
	            	|
	         	<a class="footer-colophon" href="http://www.plinga.com/corporate/terms-of-service/" target="_blank" title="Terms of Service">Terms of Service</a>
	        		|
				<a class="footer-colophon" href="http://www.plinga.com/corporate/data-privacy-policy/" target="_blank" title="Privacy Policy">Privacy Policy</a>
	  		</span>
   		</div>
</div>