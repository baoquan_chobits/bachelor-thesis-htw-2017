@guest
	<div class="text-center header">
		<form class="form-inline" action="/user/login" method="POST" role="form">
			{{ csrf_field() }}
			
			  @if ($flash = session('login-message'))
					<div id="flash-message" class="alert alert-warning" role="alert">
						{{ $flash }}
					</div>
					<div style="display:inline;margin-right:10px"></div>
			  @endif
			  
			  <div class="form-group">
				    <label class="sr-only" for="Email">Email address</label>
				    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
			  </div>
			  <div class="form-group">
				    <label class="sr-only" for="Password">Password</label>
				    <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
			  </div>
			  <input type="submit" name="login-submit" id="login-submit" class="btn btn-warning" value="Sign In">
			  <a href="#" onclick="show('RegistrationOverlay')" class="btn btn-success" id="signup">Sign Up</a>
		</form>
			  <a id="password-forgotten" href="#" onclick="show('forgottenPasswordOverlay')">Forgotten your password?</a>
			  <a id="password-reset" href="#" onclick="show('newPasswordOverlay')" style="display: none"></a>
	
	</div>
@endguest

@auth
	<div class="text-center header">
		<form class="form-inline" action="/user/logout" method="GET" role="form">
			{{ csrf_field() }}
		
			<span id="logout-form">Welcome {{ Auth::user()->name }}, you have a possibility to rate the game now!!! </span>
			<input type="submit" name="logout-submit" id="logout-submit" class="btn btn-warning" value="Sign Out">
		</form>
	</div>
@endauth