<div class="container row panel panel-heading overlay" id="forgottenPasswordOverlay">
	<h3 class="text-center" style="padding-bottom: 20px;color:#0783be;">Forgotten Password</h3>
	<form id="forgot-password-form" action="/user/forgot" method="POST" role="form">
		{{ csrf_field() }}
		
		@if ($flash = session('wrong-email-input'))
			<div id="flash-message" style="position: absolute;display:block;width:fit-content;top:-28%;left:80%;" class="alert alert-warning" role="alert">
				{{ $flash }}
				<?php
  					echo "<script> window.onload = function()
					{
					    document.getElementById('password-forgotten').click();
					} 
					</script>";
  				?>
			</div>
		@endif
		
		<div class="form-group">
			<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" required>
		</div>
		<div class="form-group row col-sm-6 col-sm-offset-3">
			<input type="submit" name="send-mail-submit" tabindex="4" class="form-control btn btn-success" value="Send Email">
		</div>
		<a style="top:-145px" class="close" onclick="hide('forgottenPasswordOverlay')"></a>
	</form>
</div>
