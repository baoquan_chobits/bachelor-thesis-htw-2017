<div class="container row panel panel-heading overlay" id="RegistrationOverlay">
	<h3 class="text-center" style="padding-bottom: 20px;color:#0783be;">User Register</h3>
	<form id="register-form" action="/user/register" method="POST" role="form">
		{{ csrf_field() }}
		
		@if ($flash = session('register-message'))
			<div id="flash-message" style="position: absolute;display:block;width:fit-content;top:-18%;left:65%;" class="alert alert-warning" role="alert">
				{{ $flash }}
				<?php
  					echo "<script> window.onload = function()
					{
					    document.getElementById('signup').click();
					} 
					</script>";
  				?>
			</div>
		@endif
		
		<div class="form-group">
			<input type="text" name="name" id="name" tabindex="1" class="form-control" placeholder="Your Name" value="" required>
		</div>
		<div class="form-group">
			<input type="email" name="email" id="email" tabindex="2" class="form-control" placeholder="Email Address" value="" required>
		</div>
		<div class="form-group">
			<input type="password" name="password" id="password" tabindex="3" class="form-control" placeholder="Password" required>
		</div>
		<div class="form-group">
			<input type="password" name="password_confirmation" id="password_confirmation" tabindex="3" class="form-control" placeholder="Confirm Password" required>
		</div>
		<div class="form-group row col-sm-6 col-sm-offset-3">
			<input type="submit" name="register-submit" tabindex="4" class="form-control btn btn-success" value="Register Now">
		</div>
		<a id='close' class="close" onclick="hide('RegistrationOverlay')"></a>
		</form>
</div>
