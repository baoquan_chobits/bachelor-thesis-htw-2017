@component('mail::message', ['data' => $data])
Hello {{ $data["user_name"] }},

Please click onto the button below to reset and create your new password!

@component('mail::button', ['url' => $data["reset_url"]])
Reset Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
