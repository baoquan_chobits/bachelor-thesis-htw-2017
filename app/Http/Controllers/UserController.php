<?php

namespace App\Http\Controllers;
use App\Mail\ResetPassword;
use App\User;
use App\Game;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Validator;
use Log;

class UserController extends Controller
{

	public function showMainPage()
	{
		$game = new Game;
		$allGames = $game->get();
		return view('MainPage')->with('allGames',$allGames);
	}

	public function login(Request $request)
	{
		// Get email and password from view
		$email = $request->get('email');
		$password = $request->get('password');

		// Try to log in by using attempt function from Laravel
		if (Auth::attempt(['email' => $email, 'password' => $password])) {

			return redirect()->back();
		}

		// Return flash message if the authentication is wrong
		session()->flash('login-message', 'Your email or password is incorrect');

		return redirect()->back()->withInput($request->only('email', 'remember'));

	}

	public function logout()
	{
		Auth::logout();
		return redirect()->back();
	}

	public function register(Request $request)
	{
		// Get all input data
		$input = Input::all();

		// Create the rules to check inputs
		$rules = [
				'name' => 'required',
				'email' => 'required|email|unique:users',
				'password' => 'required|min:3',
				'password_confirmation' => 'required|min:3|same:password',
		];

		// Create the check method for all inputs
		$validator = Validator::make($input, $rules);

		// If failed then return a flash message and redirect back to the current page
		if ($validator->fails())
		{
			$request->session()->flash('register-message', $validator->errors()->first());
			return Redirect::back()->withInput(Input::all());
		}

		// If successed then clear the session (in case flash message is saved) and create the new user, save them into database
		else
		{
			$request->session()->flush();
			$user= User::create(request(['name','email','password']));
			auth()->login($user);
			return redirect()->back();
		}
	}

	public function sendEmailReset(Request $request)
	{
		$user = User::where('email', $request->email)->first();
		$user_name = User::where('email', $request->email)->value('name');
		if ( $user )
		{
			// Create a new token to be sent to the user.
			DB::table('password_resets')->insert([
					'email' => $request->email,
					'token' => str_random(25),
					'created_at' => Carbon::now()
			]);

			$tokenData = DB::table('password_resets')
			->where('email', $request->email)->first();

			$token = $tokenData->token;
			$reset_url = env('DOMAIN_URL')."/forgot/$token";
			$email = $request->email;

			$data = array('reset_url'=>$reset_url, 'user_name'=>$user_name);

			Log::info($reset_url);
			Log::info($user_name);


			Mail::to($email)->send(new ResetPassword($data));
			Session::flush();
			Session::flash('success-sending-email', 'Please check your registered email address to reset password!');
			return Redirect::to('/');
		}
		else
		{
			$request->session()->flush();
			$request->session()->flash('wrong-email-input', 'The email you provided was not found!');
			return Redirect::back()->withInput(Input::all());
		}


	}

	public function showPasswordResetForm(Request $request, $token)
	{
		$tokenData = DB::table('password_resets')
		->where('token', $token)->first();

		if ( !$tokenData )
		{
			return Redirect::to('/');
		}

		$request->session()->flush();
		$request->session()->flash('trigger-overlay-reset-password', 'dummy trick');
		Session::put('token', $token);
		return Redirect::to('/');
	}

	public function resetPassword(Request $request)
	{
		$confirmation= Session::get('token');
		Log::info($confirmation);
		// Get all input data
		$input = Input::all();

		// Create the rules to check inputs
		$rules = [
				'password' => 'required|min:3',
				'password_confirmation' => 'required|min:3|same:password',
		];

		// Create the check method for all inputs
		$validator = Validator::make($input, $rules);

		// If failed then return a flash message and redirect back to the current page
		if ($validator->fails())
		{

			$request->session()->flash('wrong-new-reset-password-input', $validator->errors()->first());
			return Redirect::back()->withInput(Input::all());
		}

		else
		{
			$password = $request->password;
			$tokenData = DB::table('password_resets')
			->where('token', $confirmation)->first();

			$user = User::where('email', $tokenData->email)->first();

			if ( !$user ) return Redirect::to('/');

			$user->password = $password;
			$user->save();

			Auth::login($user);

			// Delete the reset token so that the user cannot clink on button to reset password again
			DB::table('password_resets')->where('email', $user->email)->delete();

			return Redirect::to('/');
		}
	}
}
