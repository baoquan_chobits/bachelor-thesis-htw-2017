<?php

namespace App\Http\Controllers;
use App\Game;
use App\RatingSystem;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

class GameController extends Controller
{
	public function show($game_id, $game_catalog, $game_name)
	{
		$game = Game::where('id', $game_id)->first();
		$url = $game->game_url;
		$nice_name = $game->nice_name;

		$up_vote = RatingSystem::where([['game_id', $game_id],['rating', '1']])->count();
		$down_vote = RatingSystem::where([['game_id', $game_id],['rating', '0']])->count();

		if (Auth::user())
		{
			if ($up_vote == 0 && $down_vote == 0)
			{
				$current_rating = 100;
				$rated = 'Please tell us your opinion!';

			}
			else
			{
				$current_rating = $up_vote/($up_vote+$down_vote)*100;
				$user_rated = RatingSystem::where([['game_id', $game_id],['user_id', Auth::user()->id]])->value('rating');
				if ($user_rated == 1 )
					$rated = 'You have made an up vote!';
				else
					$rated = 'You have made a down vote!';
			}

			$data = array('url'=>$url, 'nice_name'=>$nice_name, 'gameid'=>$game_id, 'rating'=>$current_rating, 'rated'=>$rated);
		}
		else
		{
			if ($up_vote == 0 && $down_vote == 0)
			{
				$current_rating = 100;
			}
			else
			{
				$current_rating = $up_vote/($up_vote+$down_vote)*100;
			}
			$data = array('url'=>$url, 'nice_name'=>$nice_name, 'gameid'=>$game_id, 'rating'=>$current_rating);
		}
		return View::make('GamePage')->with($data);
	}
}
