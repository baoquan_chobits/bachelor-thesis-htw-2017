<?php

namespace App\Http\Controllers;

use App\RatingSystem;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SocialController extends Controller
{

	public function thumbUp(Request $request)
	{
		$user = Auth::user();
		$update = false;
		$rate = RatingSystem::where('game_id', $request['gameid'])->first();
		if ($rate)
		{
			$update = true;
			if ($rate->rating == 0)
			{
				$rate->rating = $request['rating'];
				$rate->timestamps = false;
				$rate->update();
			}
		}

		else
		{
			$rate = new RatingSystem;

			$rate->rating = $request['rating'];
			$rate->user_id = $user->id;
			$rate->game_id = $request['gameid'];
			$rate->timestamps = false;

			$rate->save();
		}

		$data = $this->getCurrentRating($request);
		return $data;
	}

	public function thumbDown(Request $request)
	{
		$user = Auth::user();
		$update = false;
		$rate = RatingSystem::where('game_id', $request['gameid'])->first();
		if ( $rate )
		{
			$update = true;
			if ($rate->rating == 1)
			{
				$rate->rating = $request['rating'];
				$rate->timestamps = false;
				$rate->update();
			}
		}

		else
		{
			$rate = new RatingSystem;

			$rate->rating = $request['rating'];
			$rate->user_id = $user->id;
			$rate->game_id = $request['gameid'];
			$rate->timestamps = false;

			$rate->save();
		}

		$data = $this->getCurrentRating($request);
		return $data;
	}

	public function getCurrentRating(Request $request)
	{
		$up_vote = RatingSystem::where([['game_id', $request['gameid']],['rating', '1']])->count();
		$down_vote = RatingSystem::where([['game_id', $request['gameid']],['rating', '0']])->count();
		$current_rating = $up_vote/($up_vote+$down_vote)*100;
		$data = array('current_rating'=>$current_rating, 'up_vote'=>$up_vote, 'down_vote'=>$down_vote);
		return $data;
	}
}
