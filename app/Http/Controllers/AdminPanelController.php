<?php

namespace App\Http\Controllers;

use App\Game;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Log;




class AdminPanelController extends Controller
{
    public function showAdmin()
    {
    	return view('adminPanel/views/adminPage');
    }

    public function showUser()
    {
    	return view('adminPanel/views/UserPage');
    }

    public function showGames()
    {
    	$game = new Game;
    	$allGames = $game->get();
    	return View::make('adminPanel/views/gameViews/allGamePage')->with('allGames', $allGames);
    }

    public function showCreateGame()
    {
    	return view('adminPanel/views/gameViews/addGamePage');
    }

    public function createGame(Request $request)
    {
    	$input = Input::all();

    	$backend_name = $request->get('backend-name');
    	$nice_name = $request->get('nice-name');
    	$game_url = $request->get('game-url');
    	$catalog = $request->get('catalog');

    	$game = new Game;

    	$game->game_name = $backend_name;
    	$game->nice_name = $nice_name;
    	$game->game_url= $game_url;
    	$game->catalog= $catalog;

    	$game->save();

    	$request->session()->flash('game-added-message', 'A new game was added successfully');
    	return Redirect::back()->withInput($input);
    }

    public function showUpdateGame()
    {
    	$game = new Game;
    	$allGames = $game->get();
    	return View::make('adminPanel/views/gameViews/updateGamePage')->with('allGames',$allGames);
    }

    public function updateGame(Request $request)
    {
    	$backend_name = $request->get('backend-name');
    	$nice_name = $request->get('nice-name');
    	$game_url = $request->get('game-url');
    	$catalog = $request->get('catalog');

    	$selectedGame = Game::where('nice_name', $nice_name)->first();

    	$selectedGame->game_name = $backend_name;
    	$selectedGame->nice_name = $nice_name;
    	$selectedGame->game_url = $game_url;
    	$selectedGame->catalog = $catalog;

    	$selectedGame->save();

    	$request->session()->flash('game-updated-message', 'The chosen game was updated successfully');
    	return Redirect::back();
    }

    public function showRemoveGame()
    {
    	$game = new Game;
    	$allGames = $game->get();
    	return view('adminPanel/views/gameViews/removeGamePage')->with('allGames', $allGames);
    }

    public function removeGame(Request $request)
    {

    	$deletedGame =$request->get('checkboxes');

    	foreach ((array)$deletedGame as $deletedGame)
    	{
    		Game::where('game_name',$deletedGame)->delete();
    	}

    	$request->session()->flash('game-deleted-message', 'The chosen game was removed successfully');
    	return Redirect::back();
    }

    public function selectGame(Request $request)
    {
    	$nice_name = $request['game'];
    	$selectedGame = Game::where('nice_name', $nice_name)->first();

    	$game_name = $selectedGame->game_name;
    	$game_url = $selectedGame->game_url;
    	$catalog = $selectedGame->catalog;
    	$data = array('game_name'=>$game_name, 'nice_name'=>$nice_name, 'game_url'=>$game_url,'catalog'=>$catalog);

    	return $data;
    }

}
