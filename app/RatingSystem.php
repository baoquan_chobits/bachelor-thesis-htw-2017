<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RatingSystem extends Model
{
    protected $table = 'rating';
}
