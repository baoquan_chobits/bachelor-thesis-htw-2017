<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Routes for portal */

Route::get('/','UserController@showMainPage');

Route::post('/user/register', 'UserController@register');

Route::post('/user/login', 'UserController@login');

Route::get('/user/logout','UserController@logout');

Route::post('/user/forgot', 'UserController@sendEmailReset');

Route::get('forgot/{token}', 'UserController@showPasswordResetForm');

Route::post('forgot/token-confirmed', 'UserController@resetPassword');

Route::get('/game/{game_id}/{game_catalog}/{game_name}', 'GameController@show');

Route::post('/thumbUp', [
		'uses'=>'SocialController@thumbUp',
		'as'=>'thumbup'
]);

Route::post('/thumbDown', [
		'uses'=>'SocialController@thumbDown',
		'as'=>'thumbdown'
]);


/* Routes for admin dashboard */

Route::get('/admin','AdminPanelController@showAdmin');

Route::get('/admin/user','AdminPanelController@showUser');

Route::get('/admin/games','AdminPanelController@showGames');

Route::get('/admin/game/config','AdminPanelController@showUpdateGame');

Route::post('/admin/game/updateGame','AdminPanelController@updateGame');

Route::get('/admin/addgame','AdminPanelController@showCreateGame');

Route::post('/admin/game/createNewGame','AdminPanelController@createGame');

Route::get('/admin/game/remove','AdminPanelController@showRemoveGame');

Route::post('/admin/game/deleteGame','AdminPanelController@removeGame');

Route::post('/selectGame','AdminPanelController@selectGame');




